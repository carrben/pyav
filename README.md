PyAV
====

This project is forked from the fantastic work done by Mike Boers: https://github.com/mikeboers/PyAV

Docs and Gitter Chat for the original GitHub project:

[![Gitter Chat][gitter-badge]][gitter] [![Documentation][docs-badge]][docs] \

PyAV is a Pythonic binding for the [FFmpeg][ffmpeg] libraries. We aim to provide all of the power and control of the underlying library, but manage the gritty details as much as possible.

PyAV is for direct and precise access to your media via containers, streams, packets, codecs, and frames. It exposes a few transformations of that data, and helps you get your data to/from other packages (e.g. Numpy and Pillow). 

This power does come with some responsibility as working with media is horrendously complicated and PyAV can't abstract it away or make all the best decisions for you. If the `ffmpeg` command does the job without you bending over backwards, PyAV is likely going to be more of a hindrance than a help.

But where you can't work without it, PyAV is a critical tool.


Installation
------------

Version 7.x of PyAV only supports FFmpeg >= 4.0, this fork will maintain PyAV 6.x for use with FFmpeg 3.x.

This fork is not available on PyPI, and can be installed by:

1. Downloading the CI Artifacts
1. Unzipping them
1. `pip install`-ing the wheel found in the `dist` directory

this requires the FFmpeg libraries to be installe on your system, if it is Ubuntu 18.04 based this can be as easy as:

```
sudo apt install libav{codec,device,filter,format,resample,util}-dev libsw{resample,scale}-dev
```


And if you want to build from the absolute source (for development or testing):

```
git clone git@gitlab.com:carrben/pyav.git
cd PyAV
source scripts/activate
make
```

---

Have fun, [read the docs][docs], [come chat with us][gitter], and good luck!



[appveyor-badge]: https://img.shields.io/appveyor/ci/mikeboers/PyAV/develop.svg?logo=appveyor&label=appveyor
[appveyor]: https://ci.appveyor.com/project/mikeboers/pyav
[conda-badge]: https://img.shields.io/conda/vn/conda-forge/av.svg?colorB=CCB39A
[conda]: https://anaconda.org/conda-forge/av
[docs-badge]: https://img.shields.io/badge/docs-on%20mikeboers.com-blue.svg
[docs]: http://docs.mikeboers.com/pyav/develop/
[gitter-badge]: https://img.shields.io/gitter/room/nwjs/nw.js.svg?logo=gitter&colorB=cc2b5e
[gitter]: https://gitter.im/mikeboers/PyAV
[pypi-badge]: https://img.shields.io/pypi/v/av.svg?colorB=CCB39A
[pypi]: https://pypi.org/project/av
[travis-badge]: https://img.shields.io/travis/mikeboers/PyAV/develop.svg?logo=travis&label=travis
[travis]: https://travis-ci.org/mikeboers/PyAV

[github-badge]: https://img.shields.io/badge/dynamic/xml.svg?label=github&url=https%3A%2F%2Fraw.githubusercontent.com%2Fmikeboers%2FPyAV%2Fdevelop%2FVERSION.txt&query=.&colorB=CCB39A&prefix=v
[github]: https://github.com/mikeboers/PyAV

[ffmpeg]: http://ffmpeg.org/
[conda-forge]: https://conda-forge.github.io/
[conda-install]: https://conda.io/docs/install/quick.html

